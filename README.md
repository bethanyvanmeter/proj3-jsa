# proj3-JSA
Vocabulary anagrams game for primary school English language learners (ELL)

## Author: Bethany Van Meter, bvanmet2@uoregon.edu

## Overview

A simple anagram game designed for English-language learning students in elementary and middle school. Students are presented with a list of vocabulary words (taken from a text file) and an anagram. The anagram is a jumble of some number of vocabulary words, randomly chosen. Students attempt to type words that can be created from the jumble. When a matching word is typed, it is added to a list of solved words. 

The vocabulary word list is fixed for one invocation of the server, so multiple students connected to the same server will see the same vocabulary list but may  have different anagrams.

I didn't complete this project because I was working a lot this week. So sorry about that!

## Source code from 

Initial version by M Young; Docker version added by R Durairajan; to be revised by CIS 322 students. 


## To run:
Ensure credentials.ini is in the vocab folder.

cd into vocab: cd vocab/
run run.sh: ./run.sh